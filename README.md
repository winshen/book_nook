**The Book Nook**
This program takes in a user query and returns books related to the query.
Information such as titles, authors, publishers, and links to more details
are provided through the Google Books API.

## How to run the program locally from the command line
1. Download the entire project `book_nook`.

2. Navigate to the `/book_nook/project` folder.

3. From the command line, type `python3 app.py` to start the server. Then in your
browser, type `localhost:5000` to open the application.

## How to run tests
1. With the project downloaded, navigate to the `/book_nook/project/tests` folder.

2. From the command line, type `python3 <test_filename>` to run the tests.
