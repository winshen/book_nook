class BookResult():
    def __init__(self, result):
        self.info = result

    def long(self, attr):
        return len(self.info[attr]) > 100

    def shorten(self, attr):
        return self.info[attr][:100] + '...'

    def title(self):
        if self.info['title']:
            if self.long('title'):
                return self.shorten('title')
            else:
                return self.info['title']
        else:
            return 'Title unavailable'

    def author(self):
        try:
            return self.info['authors'][0]
        except KeyError:
            return 'Unknown author(s)'

    def publisher(self):
        try:
            return self.info['publisher']
        except KeyError:
            return 'Unknown publisher'

    def image(self):
        try:
            return self.info['imageLinks']['smallThumbnail']
        except KeyError:
            return 'No image available'

    def info_link(self):
        try:
            return self.info['infoLink']
        except KeyError:
            return 'No info available'

def main():
    pass

if __name__ == '__main__':
    main()
