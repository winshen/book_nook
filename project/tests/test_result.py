import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
import app
from models import result

class BookNookTests(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()
        api_response = app.data_request('Agatha Christie')
        book = api_response['items'][0]['volumeInfo']
        self.book = result.BookResult(book)

    def test_title(self):
        self.assertNotEqual('Title unavailable', self.book.title())

    def test_author(self):
        self.assertNotEqual('Unknown author(s)', self.book.author())

    def test_publisher(self):
        self.assertNotEqual('Unknown publisher', self.book.publisher())

    def test_image(self):
        self.assertNotEqual('No image available', self.book.image())

    def test_info_link(self):
        self.assertNotEqual('No info available', self.book.info_link())

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()
