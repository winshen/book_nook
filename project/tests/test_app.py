import unittest, os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
import app

class BookNookTests(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()

    def test_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'The Book Nook', response.data)

    def test_request_success(self):
        api_response = app.data_request('dogs')
        self.assertIn('items', api_response)

    def test_empty_results(self):
        '''No queries have been found to turn up empty results'''
        pass

    def test_parse_title(self):
        api_response = app.data_request('Harry Potter')
        results = app.parse(api_response)
        self.assertTrue(results[0].title())

    def test_parse_author(self):
        api_response = app.data_request('To Kill a Mockingbird')
        results = app.parse(api_response)
        self.assertTrue(results[0].author())

    def test_parse_no_publisher(self):
        api_response = app.data_request('northeast african studies')
        results = app.parse(api_response)
        self.assertEqual(results[0].publisher(), 'Unknown publisher')

    def test_page_not_found(self):
        response = self.app.get('/missing-page')
        self.assertEqual(response.status_code, 404)

    def tearDown(self):
        pass

if __name__ == "__main__":
    unittest.main()
