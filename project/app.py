from flask import Flask, render_template, request
import requests, json, logging, os, sys
sys.path.append(os.path.join(os.getcwd()))
from project.models import result

app = Flask(__name__)

@app.route('/')
def home():
    app.logger.info('Home page opened')
    return render_template('home.html')

@app.route('/search', methods = ['GET', 'POST'])
def search():
    keywords = request.args.get("term")
    raw_results = data_request(keywords)
    refined_results = parse(raw_results)
    return render_template('results.html',
            results = refined_results, empty = empty(refined_results), keywords = keywords)

def data_request(keywords):
    response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + \
                keywords.replace(' ', '%20') + \
                '&fields=kind,items/volumeInfo(title,authors,publisher,imageLinks/smallThumbnail,infoLink)')
    app.logger.info('Retrieved results for "' + keywords + '"')
    return response.json()

def empty(refined_results):
    return len(refined_results) == 0

def parse(raw_results):
    items = raw_results['items']
    return [result.BookResult(item['volumeInfo']) for item in items]

@app.errorhandler(404)
def page_not_found(error):
    app.logger.error('Page not found: ' + request.path)
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_server_error(error):
    app.logger.error('Server error: ' + error)
    return render_template('500.html'), 500

@app.errorhandler(Exception)
def internal_server_error(e):
    app.logger.error('Unhandled exception: ' + e)
    return render_template('500.html'), 500

if __name__ == '__main__':
    app.run(debug=True)
